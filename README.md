# NoobyScript

Example plain HTML and JavaScript site. Get started with local development:


## 1. Download this template

Download and unzip this template or get it with [Git](https://git-scm.com/downloads).

## 2. Open index.html in a browser

In the public folder, open the index.html with Chrome or your preferred browser.
<br/>
<br/>

That's it. You're up and running. Start making changes by editing index.html in [VS Code](https://code.visualstudio.com/) or something similar.


